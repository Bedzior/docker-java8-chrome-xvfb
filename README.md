# Dockerfile for [peptides](https://bitbucket.org/Bedzior/peptides) #
This Dockerfile is intended for an automated environment to build a solution and test it as a last step before deployment. The current version contains the following applications (with respective versions):

* **OpenJDK** 1.8.0_131
* **Maven** 3.5.0
* **Google Chrome** 59.0.3071.115